\contentsline {chapter}{Wprowadzenie}{7}
\contentsline {chapter}{\numberline {1}Podstawowe poj\IeC {\k e}cia}{9}
\contentsline {section}{\numberline {1.1}Definicje}{9}
\contentsline {chapter}{\numberline {2}Cele projektu i motywacje}{11}
\contentsline {section}{\numberline {2.1}Smartfony}{11}
\contentsline {section}{\numberline {2.2}BrightSight}{11}
\contentsline {section}{\numberline {2.3}Za\IeC {\l }o\IeC {\.z}enia aplikacji}{11}
\contentsline {chapter}{\numberline {3}Analiza rynku}{13}
\contentsline {section}{\numberline {3.1}Demografia rynku}{13}
\contentsline {section}{\numberline {3.2}Konkurencyjne rozwi\IeC {\k a}zania}{13}
\contentsline {subsection}{\numberline {3.2.1}Google Maps i jakdojade.pl}{13}
\contentsline {subsection}{\numberline {3.2.2}Georgie}{13}
\contentsline {subsection}{\numberline {3.2.3}Screen Readery}{13}
\contentsline {subsection}{\numberline {3.2.4}Talks \& Zooms}{14}
\contentsline {paragraph}{}{14}
\contentsline {chapter}{\numberline {4}Wyb\IeC {\'o}r \IeC {\'s}rodowiska}{15}
\contentsline {section}{\numberline {4.1}Wyb\IeC {\'o}r systemu operacyjnego}{15}
\contentsline {section}{\numberline {4.2}System Kontroli wersji}{15}
\contentsline {section}{\numberline {4.3}IDE}{16}
\contentsline {chapter}{\numberline {5}Organizacja pracy}{17}
\contentsline {section}{\numberline {5.1}Metodologia}{17}
\contentsline {section}{\numberline {5.2}Harmonogram prac}{18}
\contentsline {subsubsection}{Spotkania}{18}
\contentsline {subsubsection}{Pa\IeC {\'z}dziernik-Listopad 2012}{18}
\contentsline {subsubsection}{Grudzie\IeC {\'n} 2012}{18}
\contentsline {subsubsection}{Stycze\IeC {\'n} 2013}{18}
\contentsline {subsubsection}{Luty 2013}{19}
\contentsline {subsubsection}{Marzec 2013}{19}
\contentsline {subsubsection}{Kwiecie\IeC {\'n} 2013}{19}
\contentsline {subsubsection}{Maj 2013}{19}
\contentsline {subsubsection}{Czerwiec 2013}{19}
\contentsline {chapter}{\numberline {6}Interfejs}{21}
\contentsline {section}{\numberline {6.1}Menu g\IeC {\l }\IeC {\'o}wne}{22}
\contentsline {section}{\numberline {6.2}\IeC {\'S}ledzenie}{22}
\contentsline {section}{\numberline {6.3}Wyszukiwanie}{23}
\contentsline {section}{\numberline {6.4}Szczeg\IeC {\'o}\IeC {\l }y POI}{23}
\contentsline {section}{\numberline {6.5}Dodawanie}{24}
\contentsline {section}{\numberline {6.6}Wprowadzanie tekstu}{24}
\contentsline {section}{\numberline {6.7}Ustawienia}{25}
\contentsline {chapter}{\numberline {7}Przypadki u\IeC {\.z}ycia}{27}
\contentsline {section}{\numberline {7.1}Aktorzy}{27}
\contentsline {section}{\numberline {7.2}Przypadki u\IeC {\.z}ycia}{27}
\contentsline {subsection}{\numberline {7.2.1}Wyszukiwanie POI}{27}
\contentsline {subsubsection}{Aktorzy}{27}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{27}
\contentsline {subsubsection}{Kr\IeC {\'o}tki opis}{27}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci}{27}
\contentsline {subsection}{\numberline {7.2.2}Uruchomienie \IeC {\'s}ledzenia POI}{27}
\contentsline {subsubsection}{Aktorzy}{27}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{28}
\contentsline {subsubsection}{Kr\IeC {\'o}tki opis}{28}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci}{28}
\contentsline {subsection}{\numberline {7.2.3}Sprawdzanie w\IeC {\l }asnej lokacji}{28}
\contentsline {subsubsection}{Aktorzy}{28}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{28}
\contentsline {subsubsection}{Kr\IeC {\'o}tki opis}{28}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci}{28}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci alternatywne}{28}
\contentsline {subsection}{\numberline {7.2.4}Zako\IeC {\'n}czenie \IeC {\'s}ledzenia POI}{29}
\contentsline {subsubsection}{Aktorzy}{29}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{29}
\contentsline {subsubsection}{Kr\IeC {\'o}tki opis}{29}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci}{29}
\contentsline {subsection}{\numberline {7.2.5}Ustawianie alarmu na POI}{29}
\contentsline {subsubsection}{Aktorzy}{29}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{29}
\contentsline {subsubsection}{Kr\IeC {\'o}tki opis}{29}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci}{29}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci alternatywne}{29}
\contentsline {subsection}{\numberline {7.2.6}Alarm POI}{29}
\contentsline {subsubsection}{Aktorzy}{29}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{30}
\contentsline {subsubsection}{Kr\IeC {\'o}tki opis}{30}
\contentsline {subsection}{\numberline {7.2.7}Dodawanie POI}{30}
\contentsline {subsubsection}{Aktorzy}{30}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{30}
\contentsline {subsubsection}{Kr\IeC {\'o}tki opis}{30}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci}{30}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci alternatywne}{30}
\contentsline {subsection}{\numberline {7.2.8}Usuwanie POI}{31}
\contentsline {subsubsection}{Aktorzy}{31}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{31}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci}{31}
\contentsline {subsection}{\numberline {7.2.9}Zmiana g\IeC {\l }o\IeC {\'s}no\IeC {\'s}ci czytanych komunikat\IeC {\'o}w}{31}
\contentsline {subsubsection}{Aktorzy}{31}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{31}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci}{31}
\contentsline {subsection}{\numberline {7.2.10}Zmiana cz\IeC {\k e}stotliwo\IeC {\'s}ci aktualizacji lokalizacji}{31}
\contentsline {subsubsection}{Aktorzy}{31}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{31}
\contentsline {subsubsection}{Kr\IeC {\'o}tki opis}{32}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci}{32}
\contentsline {subsection}{\numberline {7.2.11}Zmiana tempa czytania komunikat\IeC {\'o}w}{32}
\contentsline {subsubsection}{Aktorzy}{32}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{32}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci}{32}
\contentsline {subsection}{\numberline {7.2.12}W\IeC {\l }\IeC {\k a}czenie lub wy\IeC {\l }\IeC {\k a}czenie wibracji}{32}
\contentsline {subsubsection}{Aktorzy}{32}
\contentsline {subsubsection}{Za\IeC {\l }o\IeC {\.z}enia}{32}
\contentsline {subsubsection}{Kr\IeC {\'o}tki opis}{32}
\contentsline {subsubsection}{Czynno\IeC {\'s}ci}{33}
\contentsline {chapter}{\numberline {8}Architektura}{35}
\contentsline {section}{\numberline {8.1}Rozpoznawanie gest\IeC {\'o}w od BrightSight}{36}
\contentsline {section}{\numberline {8.2}Ustawienia}{36}
\contentsline {section}{\numberline {8.3}Google Maps}{36}
\contentsline {section}{\numberline {8.4}Widoki}{36}
\contentsline {section}{\numberline {8.5}Mowa}{38}
\contentsline {section}{\numberline {8.6}Baza POI}{38}
\contentsline {section}{\numberline {8.7}Baza lokalizacji}{39}
\contentsline {section}{\numberline {8.8}Po\IeC {\l }o\IeC {\.z}enie - us\IeC {\l }uga w tle}{39}
\contentsline {section}{\numberline {8.9}Alarmy}{39}
\contentsline {section}{\numberline {8.10}Kierunki \IeC {\'s}wiata}{39}
\contentsline {chapter}{\numberline {9}Testowanie aplikacji}{41}
\contentsline {section}{\numberline {9.1}Ola Safianowska}{41}
\contentsline {section}{\numberline {9.2}Niekt\IeC {\'o}re decyzje podj\IeC {\k e}te po konsultacjach z Ol\IeC {\k a}}{41}
\contentsline {section}{\numberline {9.3}Rady do produkcji systemu}{42}
\contentsline {section}{\numberline {9.4}Pozytywne wra\IeC {\.z}enia}{42}
\contentsline {chapter}{\numberline {10}Podzia\IeC {\l } prac}{43}
\contentsline {section}{\numberline {10.1}Anna Sza\IeC {\l }ygin}{43}
\contentsline {section}{\numberline {10.2}Mateusz Kitlas}{43}
\contentsline {section}{\numberline {10.3}Wojciech Chmiel}{44}
\contentsline {section}{\numberline {10.4}Zofia Ostoja-Starzycka}{44}
\contentsline {chapter}{\numberline {11}Podsumowanie}{45}
\contentsline {section}{\numberline {11.1}Pomys\IeC {\l }y na rozw\IeC {\'o}j aplikacji}{45}
\contentsline {section}{\numberline {11.2}Perspektywy na przysz\IeC {\l }o\IeC {\'s}\IeC {\'c}}{45}
\contentsline {chapter}{\numberline {A}Projekty us\IeC {\l }ugi lokalizacyjnej}{47}
\contentsline {section}{\numberline {A.1}Maszty GSM}{47}
\contentsline {chapter}{\numberline {B}Uruchamianie aplikacji}{49}
\contentsline {section}{\numberline {B.1}Wymagania}{49}
\contentsline {section}{\numberline {B.2}Instrukcja}{49}
\contentsline {chapter}{\numberline {C}Inne rozwi\IeC {\k a}zania}{51}
\contentsline {chapter}{Bibliografia}{53}
